<?php





class db
{
    private $db_host = "localhost";
    private $db_user = "id15484353_admin";
    private $db_password = "cb\}|>]mH*>wi+^8";
    private $db_name = "id15484353_db";
    private $mysqli;

    public $isError = false;
    public $message = "";

    /**
     * Инициализация подключения к базе данных
     */
    function  __construct()
    {
        $this->mysqli = new mysqli($this->db_host, $this->db_user, $this->db_password, $this->db_name);
        if ($this->mysqli->connect_errno) {
            $error = "Не удалось подключиться к MySQL: (" . $this->mysqli->connect_errno . ") " . $this->mysqli->connect_error;
            $this->writeLog($error);
        }
    }

    /**
     * Запись ошибки в файл лога
     */
    private function writeLog($message)
    {
        try {
            $filename = 'log_' . date('YmdHis') . '.txt';
            $fp = fopen("logs//" . $filename, 'w');
            fwrite($fp, $message);
            fclose($fp);
        } catch (Exception $e) {
            echo "Критическая ошибка!<br> $e";
            die();
            exit();
        }
    }



    /**
     * Получить группы учеников
     */
    public function getGroups()
    {
        $groups = [];

        $cmd = "SELECT * FROM `groups`;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $groups[] = $obj;
            }
            $res->close();
        }
        return $groups;
    }

    /**
     * Получить группу по идентификатору 
     */
    public function getGroupById($id)
    {
        $group = false;
        $cmd = "SELECT * FROM `groups` WHERE `id` = $id;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $group = $obj;
                break;
            }
            $res->close();
        }
        return $group;
    }


    /**
     * Получить список ролей
     */
    public function getRoles()
    {
        $roles = [];
        $cmd = "SELECT * FROM `roles`;";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $roles[] = $obj;
            }
            $res->close();
        }
        return $roles;
    }

    /**
     * Получить роль
     * @param id Идентификатор пользователя
     * @return class|FALSE
     */
    public function getRole($id)
    {
        $role = false;
        $cmd = sprintf("SELECT * FROM `roles` WHERE `id` = '%s';", $id);
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $role = $obj;
                break;
            }
            $res->close();
        }
        return $role;
    }




    /**
     * Создание нового пользователя 
     */
    public function createUser($email, $password, $username, $role, $group)
    {

        $cmd = "INSERT INTO `users` (`email`, `password`, `name`, `role`, `group`, `created_dt`, `update_dt`)";
        $cmd .= " VALUES ('$email', '$password', '$username', '$role', '$group', current_timestamp(), current_timestamp())";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при создание новой учетной записи.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            $error = "";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($error);
            return false;
        }
    }

    /**
     * Поиск пользователя
     * @return object|false Возвращает класс пользователя, соответствующий таблице в БД или FALSE если пользователь не найден
     */
    public function findUser($email, $password = '')
    {
        $user = false;
        if ($password == '') {
            $cmd = "SELECT * FROM `users` WHERE `email` = '$email'";
        } else {
            $cmd = "SELECT * FROM `users` WHERE `email` = '$email' and `password` = '$password'";
        }
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $user = $obj;
                break;
            }
            $res->close();
        }
        return $user;
    }
    /**
     * Получить пользователя по его идентификатору 
     * @return object|false Возвращает класс пользователя, соответствующий таблице в БД или FALSE если пользователь не найден
     */
    public function getUserById($id)
    {
        $user = false;
        $cmd = "SELECT * FROM `users` WHERE `id` = '$id'";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $user = $obj;
                break;
            }
            $res->close();
        }
        return $user;
    }


    /**
     * Получить список пользователей учебной группы
     * @param id Идентификатор учебной группы
     * @param role идентификатор роли пользователя
     * @return array Массив объектов
     */
    public function getUsersByGroup($id, $role = 1)
    {
        $users = [];
        $cmd = "SELECT * FROM `users` WHERE `group` in ($id) AND `role` in ($role)";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $users[] = $obj;
            }
            $res->close();
        }
        return $users;
    }


    /**
     * Поиск роли пользователя по интенсификатору
     * @return object|false Возвращает класс роли, соответствующий таблице в БД или FALSE если роль не найдена
     */
    public function findRole($id)
    {
        $role = false;
        $cmd = "SELECT * FROM `roles` WHERE `id` like $id";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $role = $obj;
                break;
            }
            $res->close();
        }
        return $role;
    }
    /**
     * Обновление данных пользователя
     */
    public function updateUser($id, $email, $password, $name, $role, $group)
    {
        $cmd = "UPDATE `users` SET `email` = '$email', `password` = '$password', `name` = '$name', `role` = '$role', `group` = '$group' WHERE `users`.`id` = $id";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при создание новой учетной записи.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            $error = "";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            $this->writeLog($error);
            return false;
        }
    }


    /**
     * Получить все задания
     */
    public function getTasks()
    {
        $tasks = [];
        $cmd = "SELECT * FROM `tasks`";


        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
        return $tasks;
    }

    /**
     * Получить все задания по автору
     */
    public function getTasksByAuthors($ids)
    {
        $tasks = [];
        $cmd = sprintf("SELECT * FROM `tasks` WHERE `author` in (%s);", implode(',', $ids));
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $tasks[] = $obj;
            }
            $res->close();
        }
        return $tasks;
    }


    /**
     * Получить задание по идентификатору
     * @param id идентификатор задания
     * @return class|false 
     */
    public function getTask($id)
    {
        $task = false;
        $cmd = "SELECT * FROM `tasks` WHERE `id` = $id";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $task = $obj;
                break;
            }
            $res->close();
        }
        return $task;
    }

    /**
     * Получить все ответы студента
     * @param id Идентификатор пользователя
     * @return array Массив 
     */
    public function getAnswers()
    {
        $answers = [];
        $cmd = "SELECT * FROM `answers`";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $answers[] = $obj;
            }
            $res->close();
        }
        return $answers;
    }
    /**
     * Получить все ответы студента
     * @param id Идентификатор пользователя
     * @return array Массив 
     */
    public function getAnswersByUser($id)
    {
        $answers = [];
        $cmd = "SELECT * FROM `answers` WHERE `user` in ($id)";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $answers[] = $obj;
            }
            $res->close();
        }
        return $answers;
    }
    /**
     * Получить ответ
     * @param id Идентификатор ответа
     * @return array Массив 
     */
    public function getAnswer($id)
    {
        $answer = false;
        $cmd = "SELECT * FROM `answers` WHERE `id` in ($id)";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $answer = $obj;
                break;
            }
            $res->close();
        }
        return $answer;
    }



    /**
     * Получить ответ студента по заданию
     * @param id Идентификатор пользователя
     * @return class|FALSE 
     */
    public function getAnswerByUserAndTask($user, $task)
    {
        $answer = false;
        $cmd = "SELECT * FROM `answers` WHERE `user` = $user AND `task` = $task";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $answer = $obj;
                break;
            }
            $res->close();
        }
        return $answer;
    }



    /**
     * Создать ответ
     * @param user Идентификатор пользователя
     * @param task Идентификатор задания
     * @param res Содержимое ответа
     * @return TRUE|FALSE При успешном создание нового ответа возвращает TRUE, иначе FALSE
     */
    public function createAnswer($user, $task, $res)
    {
        $cmd = "INSERT INTO `answers` (`user`, `task`, `created_dt`, `update_dt`, `res`) VALUES ('$user', '$task', current_timestamp(), current_timestamp(), '$res');";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при добавление ответа.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            return false;
        }
    }

    /**
     * Обновить ответ
     * @param id Идентификатор ответа
     * @param res Содержимое ответа
     * @return TRUE|FALSE При успешном создание нового ответа возвращает TRUE, иначе FALSE
     */
    public function updateAnswer($id, $res)
    {
        $cmd = "UPDATE `answers` SET `update_dt` = current_timestamp(), `res` = '$res' WHERE `answers`.`id` = $id";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при обновление ответа.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            return false;
        }
    }


    /**
     * 
     */
    public function createTask($data)
    {
        $author = $data['user'];
        $text = $data['text'];
        $answer = $data['answer'];
        $xa1 = $data['xa1'];
        $ya1 = $data['ya1'];
        $za1 = $data['za1'];
        $xa2 = $data['xa2'];
        $ya2 = $data['ya2'];
        $za2 = $data['za2'];
        $xb1 = $data['xb1'];
        $yb1 = $data['yb1'];
        $zb1 = $data['zb1'];
        $xb2 = $data['xb2'];
        $yb2 = $data['yb2'];
        $zb2 = $data['zb2'];
        $xc1 = $data['xc1'];
        $yc1 = $data['yc1'];
        $zc1 = $data['zc1'];
        $xc2 = $data['xc2'];
        $yc2 = $data['yc2'];
        $zc2 = $data['zc2'];
        $xd1 = $data['xd1'];
        $yd1 = $data['yd1'];
        $zd1 = $data['zd1'];
        $xd2 = $data['xd2'];
        $yd2 = $data['yd2'];
        $zd2 = $data['zd2'];
        $xe1 = $data['xe1'];
        $ye1 = $data['ye1'];
        $ze1 = $data['ze1'];
        $xe2 = $data['xe2'];
        $ye2 = $data['ye2'];
        $ze2 = $data['ze2'];
        $xf1 = $data['xf1'];
        $yf1 = $data['yf1'];
        $zf1 = $data['zf1'];
        $xf2 = $data['xf2'];
        $yf2 = $data['yf2'];
        $zf2 = $data['zf2'];
        $xl1 = $data['xl1'];
        $yl1 = $data['yl1'];
        $zl1 = $data['zl1'];
        $xl2 = $data['xl2'];
        $yl2 = $data['yl2'];
        $zl2 = $data['zl2'];
        $xl3 = $data['xl3'];
        $yl3 = $data['yl3'];
        $zl3 = $data['zl3'];
        $xl4 = $data['xl4'];
        $yl4 = $data['yl4'];
        $zl4 = $data['zl4'];
        $cmd = "INSERT INTO `tasks` ( `created_dt`, `update_dt`, `author`, `text`, `xa1`, `ya1`, `za1`, `xa2`, `ya2`, `za2`, `xb1`, `yb1`, `zb1`, `xb2`, `yb2`, `zb2`, `xc1`, `yc1`, `zc1`, `xc2`, `yc2`, `zc2`, `xd1`, `yd1`, `zd1`, `xd2`, `yd2`, `zd2`, `xe1`, `ye1`, `ze1`, `xe2`, `ye2`, `ze2`, `xf1`, `yf1`, `zf1`, `xf2`, `yf2`, `zf2`, `xl1`, `yl1`, `zl1`, `xl2`, `yl2`, `zl2`, `xl3`, `yl3`, `zl3`, `xl4`, `yl4`, `zl4`, `answer`)";
        $cmd .= " VALUES (current_timestamp(), current_timestamp(),'$author','$text','$xa1', '$ya1', '$za1', '$xa2', '$ya2', '$za2', '$xb1', '$yb1', '$zb1', '$xb2', '$yb2', '$zb2', '$xc1', '$yc1', '$zc1', '$xc2', '$yc2', '$zc2', '$xd1', '$yd1', '$zd1', '$xd2', '$yd2', '$zd2', '$xe1', '$ye1', '$ze1', '$xe2', '$ye2', '$ze2', '$xf1', '$yf1', '$zf1', '$xf2', '$yf2', '$zf2', '$xl1', '$yl1', '$zl1', '$xl2', '$yl2', '$zl2', '$xl3', '$yl3', '$zl3', '$xl4', '$yl4', '$zl4', '$answer')";
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при добавление задания.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            return false;
        }
    }
    /**
     * Удалить задание
     * @param id Идентификатор задания
     * @return TRUE|FALSE При успешном создание нового ответа возвращает TRUE, иначе FALSE
     */
    public function removeTask($id)
    {
        $cmd = sprintf("DELETE FROM `tasks` WHERE `tasks`.`id` = %s", $id);
        if ($this->mysqli->query($cmd) === TRUE) {
            return true;
        } else {
            $this->isError = true;
            $this->message = "Возникла ошибка при удаление задания.<br>";
            $this->message .= "sql: " . $cmd . "<br>";
            if ($this->mysqli->connect_errno) {
                $this->message .= $this->mysqli->connect_errno . "<br>";
                $this->message .= $this->mysqli->connect_error . "<br>";
            }
            return false;
        }
    }

    /**
     * Получить список идентификаторов пользователей, которые выполняли задания
     * @return array
     */
    public function getUsersInAnswer()
    {
        $userIds = [];
        $cmd = "SELECT `user` FROM `answers` WHERE 1 GROUP BY `user`";
        if ($res = $this->mysqli->query($cmd)) {
            while ($obj = $res->fetch_object()) {
                $userIds[] = $obj->user;
            }
            $res->close();
        }
        return $userIds;
    }
}


$db = new db();
