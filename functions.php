<?php

$isLogged = false;
if (isset($_SESSION['user'])) {
    $isLogged = true;
}

$messageSuccess  = "";
$messageWarning  = "";
$messageError  = "";




/*******************************************/
/*******************************************/
/*******************************************/

function setMessageSuccess($text)
{
    global $messageSuccess;
    global $messageWarning;
    global $messageError;
    $messageSuccess = $text;
    $messageWarning = "";
    $messageError = "";
}
function setMessageWarning($text)
{
    global $messageSuccess;
    global $messageWarning;
    global $messageError;
    $messageSuccess = "";
    $messageWarning = $text;
    $messageError = "";
}
function setMessageError($text)
{
    global $messageSuccess;
    global $messageWarning;
    global $messageError;
    $messageSuccess = "";
    $messageWarning = "";
    $messageError = $text;
}

/**
 * Регистрация нового пользователя
 */
function reg($post)
{
    global $db;
    global $isLogged;
    $user = $db->findUser($post['email']);
    if ($user != false) {
        setMessageWarning("Пользователь с таким адресом электронной почты уже зарегистрировав");
        return false;
    }


    if ($db->createUser($post['email'], $post['password'], $post['name'], $post['role'], $post['group'])) {
        $user = $db->findUser($post['email'], $post['password']);
        $_SESSION['user'] = [];
        $_SESSION['user']['id'] = $user->id;
        $_SESSION['user']['email'] = $user->email;
        $_SESSION['user']['name'] = $user->name;
        $_SESSION['user']['password'] = $user->password;
        $_SESSION['user']['role'] = $user->role;
        $_SESSION['user']['group'] = $user->group;
        $_SESSION['user']['created_dt'] = $user->created_dt;
        $_SESSION['user']['update_dt'] = $user->update_dt;
        $isLogged = true;
        setMessageSuccess("Учетная запись успешно создана!");
    } else {
        $isLogged = false;
        setMessageError($db->message);
    }
}

/**
 * Авторизация
 */
function login($post)
{
    global $db;
    global $isLogged;
    $email = $post['email'];
    $password = $post['password'];
    $user = $db->findUser($email, $password);
    if ($user == false) {
        $isLogged = false;
        session_unset();
        setMessageWarning("Не удалось выполнить вход. Возможно, был введен неверный логин или пароль.");
    } else {
        $isLogged = true;
        $_SESSION['user'] = [];
        $_SESSION['user']['id'] = $user->id;
        $_SESSION['user']['email'] = $user->email;
        $_SESSION['user']['password'] = $user->password;
        $_SESSION['user']['name'] = $user->name;
        $_SESSION['user']['role'] = $user->role;
        $_SESSION['user']['group'] = $user->group;
        $_SESSION['user']['created_dt'] = $user->created_dt;
        $_SESSION['user']['update_dt'] = $user->update_dt;
    }
}
/**
 * Редактирование профиля пользователя
 */
function updateProfile($post)
{
    global $db;
    if ($db->updateUser($post['id'], $post['email'], $post['password'], $post['name'], $post['role'], $post['group'])) {
        $user = $db->findUser($post['email'], $post['password']);
        $_SESSION['user'] = [];
        $_SESSION['user']['id'] = $user->id;
        $_SESSION['user']['email'] = $user->email;
        $_SESSION['user']['name'] = $user->name;
        $_SESSION['user']['password'] = $user->password;
        $_SESSION['user']['role'] = $user->role;
        $_SESSION['user']['group'] = $user->group;
        $_SESSION['user']['created_dt'] = $user->created_dt;
        $_SESSION['user']['update_dt'] = $user->update_dt;
        setMessageSuccess("Учетная запись успешно обновлена!");
    } else {
        setMessageError($db->message);
    }
}


/**
 * Получить список заданий учебной группы
 */
function getTaskForGroup($id)
{
    global $db;
    $tasks = [];
    $users = $db->getUsersByGroup($id, 1);
    if (count($users) == 0) {
        return $tasks;
    }
    $usersIds = [];
    foreach ($users as $key => $user) {
        $usersIds[] = $user->id;
    }
    $tasks = $db->getTasksByAuthors($usersIds);
    return $tasks;
}

/**
 * Добавление нового ответа
 */
function createAnswer($post)
{
    global $db;
    if ($db->getAnswerByUserAndTask($post['user'], $post['task']) == false) {
        if ($db->createAnswer($post['user'], $post['task'], $post['res'])) {
            setMessageSuccess("Ответ успешно добавлен!");
        } else {
            setMessageError($db->message);
        }
    } else {
        setMessageWarning("Ответ по данному заданию уже был добавлен ранее");
    }
}
/**
 * Обновление нового ответа
 */
function updateAnswer($post)
{
    global $db;
    if ($db->updateAnswer($post['id'], $post['res'])) {
        setMessageSuccess("Ответ успешно обновлен!");
    } else {
        setMessageError($db->message);
    }
}

/**
 * Проверка задания
 */
function checkAnswer($answerId, $taskId)
{
    global $db;
    $answer = $db->getAnswer($answerId);
    if ($answer == false) {
        return false;
    }
    $task = $db->getTask($taskId);
    if ($task == false) {
        return false;
    }
    if ($answer->res == $task->answer) {
        return true;
    } else {
        return false;
    }
}


/**
 * Добавить нового задание
 */
function createTask($post)
{
    global $db;
    if ($db->createTask($post)) {
        setMessageSuccess("Задание успешно обновлено!");
    } else {
        setMessageError($db->message);
    }
}
/**
 * Удалить задание
 */
function removeTask($id)
{
    global $db;
    if ($db->removeTask($id)) {
        setMessageSuccess("Задание удалено!");
    } else {
        setMessageError($db->message);
    }
}

/**
 * Получить всех пользователей, которые выполняли задания 
 */
function getUsersInAnswers()
{
    global $db;
    $users = [];
    foreach ($db->getUsersInAnswer() as $key => $id) {
        $users[] = $db->getUserById($id);
    }
    return $users;
}
