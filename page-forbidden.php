<div class="row site-style-default pt-3 pb-3">
    <div class="col-md-4 d-flex justify-content-center align-items-center">
        <img src="assets/img/icon-forbidden.png" style="height: 250px;" alt="">
    </div>
    <div class="col-md-8 d-flex justify-content-center align-items-center">
        <h3 class="display-3 m-3 text-center">
            Доступ запрещен
        </h3>
    </div>
</div>
<div class="row">
    <div class="col-12 d-flex justify-content-center">
        <a href="/" class="btn btn-lg btn-primary m-3 p-3">На главную</a>
    </div>
</div>