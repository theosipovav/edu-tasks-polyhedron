-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Ноя 26 2020 г., 23:22
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.3.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `id15484353_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `answers`
--

CREATE TABLE `answers` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `user` int(11) NOT NULL COMMENT 'Автор ответа',
  `task` int(11) DEFAULT NULL COMMENT 'Задание',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи',
  `res` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT 'Ответ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Ответы пользователей';

--
-- Дамп данных таблицы `answers`
--

INSERT INTO `answers` (`id`, `user`, `task`, `created_dt`, `update_dt`, `res`) VALUES
(1, 1, 32, '2020-11-26 13:08:36', '2020-11-26 13:08:36', '60'),
(2, 1, 33, '2020-11-26 13:30:48', '2020-11-26 13:30:48', '3'),
(3, 5, 22, '2020-11-26 13:36:16', '2020-11-26 13:44:13', '60'),
(4, 5, 33, '2020-11-26 13:48:56', '2020-11-26 13:48:56', '10'),
(5, 9, 60, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '1'),
(6, 9, 60, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '1'),
(7, 9, 33, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '60'),
(8, 9, 34, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '60'),
(9, 5, 34, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '60'),
(10, 5, 32, '2020-11-26 22:48:05', '2020-11-26 22:48:05', '60');

-- --------------------------------------------------------

--
-- Структура таблицы `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Наименование',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Учебные группы';

--
-- Дамп данных таблицы `groups`
--

INSERT INTO `groups` (`id`, `name`, `created_dt`, `update_dt`) VALUES
(1, '2020А1', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(2, '2020А2', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(3, '2020А3', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(4, '2020Б1', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(5, '2020Б2', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(6, '2020Б3', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(7, '2020В1', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(8, '2020В2', '2020-11-23 23:00:28', '2020-11-23 23:00:28'),
(9, '2020В3', '2020-11-23 23:00:28', '2020-11-23 23:00:28');

-- --------------------------------------------------------

--
-- Структура таблицы `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Наименование',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Роли пользователей';

--
-- Дамп данных таблицы `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_dt`, `update_dt`) VALUES
(1, 'Преподаватель', '2020-11-23 22:54:44', '2020-11-23 22:54:44'),
(2, 'Студент', '2020-11-23 22:54:44', '2020-11-23 22:54:44');

-- --------------------------------------------------------

--
-- Структура таблицы `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи',
  `author` int(11) NOT NULL COMMENT 'Автор задания',
  `text` text COLLATE utf8mb4_unicode_ci DEFAULT 'NULL' COMMENT 'Текст задачи',
  `xa1` int(11) DEFAULT 0,
  `ya1` int(11) DEFAULT 0,
  `za1` int(11) DEFAULT 0,
  `xa2` int(11) DEFAULT 0,
  `ya2` int(11) DEFAULT 2,
  `za2` int(11) DEFAULT 0,
  `xb1` int(11) DEFAULT 2,
  `yb1` int(11) DEFAULT 0,
  `zb1` int(11) DEFAULT 0,
  `xb2` int(11) DEFAULT 2,
  `yb2` int(11) DEFAULT 2,
  `zb2` int(11) DEFAULT 0,
  `xc1` int(11) DEFAULT 2,
  `yc1` int(11) DEFAULT 0,
  `zc1` int(11) DEFAULT 1,
  `xc2` int(11) DEFAULT 2,
  `yc2` int(11) DEFAULT 2,
  `zc2` int(11) DEFAULT 1,
  `xd1` int(11) DEFAULT 1,
  `yd1` int(11) DEFAULT 0,
  `zd1` int(11) DEFAULT 2,
  `xd2` int(11) DEFAULT 1,
  `yd2` int(11) DEFAULT 2,
  `zd2` int(11) DEFAULT 2,
  `xe1` int(11) DEFAULT -1,
  `ye1` int(11) DEFAULT 0,
  `ze1` int(11) DEFAULT 2,
  `xe2` int(11) DEFAULT -1,
  `ye2` int(11) DEFAULT 2,
  `ze2` int(11) DEFAULT 2,
  `xf1` int(11) DEFAULT -1,
  `yf1` int(11) DEFAULT 0,
  `zf1` int(11) DEFAULT 1,
  `xf2` int(11) DEFAULT -1,
  `yf2` int(11) DEFAULT 2,
  `zf2` int(11) DEFAULT 1,
  `xl1` int(11) DEFAULT 2,
  `yl1` int(11) DEFAULT 0,
  `zl1` int(11) DEFAULT 0,
  `xl2` int(11) DEFAULT -1,
  `yl2` int(11) DEFAULT 0,
  `zl2` int(11) DEFAULT 2,
  `xl3` int(11) DEFAULT 2,
  `yl3` int(11) DEFAULT 0,
  `zl3` int(11) DEFAULT 1,
  `xl4` int(11) DEFAULT -1,
  `yl4` int(11) DEFAULT 0,
  `zl4` int(11) DEFAULT 2,
  `answer` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '60' COMMENT 'Правильный ответ'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Учебные задания';

--
-- Дамп данных таблицы `tasks`
--

INSERT INTO `tasks` (`id`, `created_dt`, `update_dt`, `author`, `text`, `xa1`, `ya1`, `za1`, `xa2`, `ya2`, `za2`, `xb1`, `yb1`, `zb1`, `xb2`, `yb2`, `zb2`, `xc1`, `yc1`, `zc1`, `xc2`, `yc2`, `zc2`, `xd1`, `yd1`, `zd1`, `xd2`, `yd2`, `zd2`, `xe1`, `ye1`, `ze1`, `xe2`, `ye2`, `ze2`, `xf1`, `yf1`, `zf1`, `xf2`, `yf2`, `zf2`, `xl1`, `yl1`, `zl1`, `xl2`, `yl2`, `zl2`, `xl3`, `yl3`, `zl3`, `xl4`, `yl4`, `zl4`, `answer`) VALUES
(22, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(32, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(33, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(34, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(35, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(36, '2020-11-26 11:25:00', '2020-11-26 11:25:00', 1, 'В правильном шестиугольном многограннике A1B1C1D1E1F1A2B2C2D2E2F2 все ребра равны 20. Найдите угол C1B1E1. Ответ дайте в градусах', 0, 0, 0, 0, 2, 0, 2, 0, 0, 2, 2, 0, 2, 0, 1, 2, 2, 1, 1, 0, 2, 1, 2, 2, -1, 0, 2, -1, 2, 2, -1, 0, 1, -1, 2, 1, 2, 0, 0, -1, 0, 2, 2, 0, 1, -1, 0, 2, '60'),
(37, '2020-11-26 16:30:23', '2020-11-26 16:30:23', 5, '1', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '2');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Идентификатор',
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Электронный адрес',
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Пароль для входа в систему',
  `name` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Имя пользователя для входа в систему',
  `role` int(11) NOT NULL COMMENT 'Роль пользователя',
  `created_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата создание записи',
  `update_dt` datetime NOT NULL DEFAULT current_timestamp() COMMENT 'Дата обновления записи',
  `group` int(11) NOT NULL DEFAULT 1 COMMENT 'Учебная группа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='Пользователи';

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `role`, `created_dt`, `update_dt`, `group`) VALUES
(1, 't1@mail.ru', '123', 'Иванов И.И.', 1, '2020-11-23 22:54:08', '2020-11-23 22:54:08', 1),
(2, 't2@mail.ru', '123', 'Петров П.П.', 1, '2020-11-23 22:54:08', '2020-11-23 22:54:08', 2),
(3, 't3@mail.ru', '123', 'Егоров Е.Е.', 1, '2020-11-23 22:54:08', '2020-11-23 22:54:08', 1),
(5, 's1@mail.ru', '123', 'Игров И.И.', 2, '2020-11-25 21:53:43', '2020-11-25 21:53:43', 1),
(8, 's2@mail.ru', '123', 'Петров Петр Петрович', 2, '2020-11-26 22:06:35', '2020-11-26 22:06:35', 1),
(9, 's3@mail.ru', '123', 'Петров Петр Петрович', 2, '2020-11-26 22:06:35', '2020-11-26 22:06:35', 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `answers`
--
ALTER TABLE `answers`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tasks_FK` (`author`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `users_FK` (`role`),
  ADD KEY `users_FK_1` (`group`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `answers`
--
ALTER TABLE `answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT для таблицы `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT для таблицы `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор', AUTO_INCREMENT=10;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_FK` FOREIGN KEY (`author`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `tasks_FK_1` FOREIGN KEY (`author`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_FK` FOREIGN KEY (`role`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users_FK_1` FOREIGN KEY (`group`) REFERENCES `groups` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
