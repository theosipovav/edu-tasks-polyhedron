$(document).ready(function () {
    $("#FormLogIn .link-of-login-in-reg").click(function (e) {
        e.preventDefault();
        $('#ModalLogIn').modal('hide');
        $('#ModalRegistration').modal('show');
    });
    /**
     * Проверка паролей при регистрации
     */
    $("#FormRegistration").submit(function (e) {
        var p1 = $("#InputRegistrationPassword").val();
        var p2 = $("#InputRegistrationPasswordRepeat").val();
        if (p1 != p2) {
            isValid = false;
            $("#InputRegistrationPassword").addClass("is-invalid");
            $("#InputRegistrationPasswordRepeat").addClass("is-invalid");
            alert("Введенные пароли не совпадают");
            e.preventDefault();
        }
    });
    /**
     * Подтверждение действия
     */
    $(".btn-confirm").click(function (e) {
        if (confirm("Вы подтверждаете операцию?")) {
        } else {
            e.preventDefault();
        }
    });
    /**
     * Поиск элемента содержащие другие данных точек
     */
    var formTask = $(".form-task");
    if (formTask.length > 0) {
        if ($("#canvas").length > 0) {
            renderPolyhedron();
        }
    }
    /**
     * Динамическая отрисовка графика при изменений параметров
     */
    $(".form-task input").change(function (e) {
        renderPolyhedron();
    });
});

/**
 * Отрисовка фигуры
 */
function renderPolyhedron() {
    var x0 = 150;
    var y0 = 350;
    var z0 = 0;

    var za1 = z0 + parseInt($(".form-task input[name=za1]").val()) * 40;
    var xa1 = za1 + x0 + parseInt($(".form-task input[name=xa1]").val()) * 100;
    var ya1 = -za1 + y0 - parseInt($(".form-task input[name=ya1]").val()) * 100;
    var za2 = -40 + z0 + parseInt($(".form-task input[name=za2]").val()) * 40;
    var xa2 = za2 + x0 + parseInt($(".form-task input[name=xa2]").val()) * 100 + 40;
    var ya2 = -za2 + y0 - parseInt($(".form-task input[name=ya2]").val()) * 100 - 40;

    var zb1 = z0 + parseInt($(".form-task input[name=zb1]").val()) * 40;
    var xb1 = zb1 + x0 + parseInt($(".form-task input[name=xb1]").val()) * 100;
    var yb1 = -zb1 + y0 - parseInt($(".form-task input[name=yb1]").val()) * 100;
    var zb2 = -40 + z0 + parseInt($(".form-task input[name=zb2]").val()) * 40;
    var xb2 = zb2 + x0 + parseInt($(".form-task input[name=xb2]").val()) * 100 + 40;
    var yb2 = -zb2 + y0 - parseInt($(".form-task input[name=yb2]").val()) * 100 - 40;

    var zc1 = z0 + parseInt($(".form-task input[name=zc1]").val()) * 40;
    var xc1 = zc1 + x0 + parseInt($(".form-task input[name=xc1]").val()) * 100;
    var yc1 = -zc1 + y0 - parseInt($(".form-task input[name=yc1]").val()) * 100;
    var zc2 = -40 + z0 + parseInt($(".form-task input[name=zc2]").val()) * 40;
    var xc2 = zc2 + x0 + parseInt($(".form-task input[name=xc2]").val()) * 100 + 40;
    var yc2 = -zc2 + y0 - parseInt($(".form-task input[name=yc2]").val()) * 100 - 40;

    var zd1 = z0 + parseInt($(".form-task input[name=zd1]").val()) * 40;
    var xd1 = zd1 + x0 + parseInt($(".form-task input[name=xd1]").val()) * 100;
    var yd1 = -zd1 + y0 - parseInt($(".form-task input[name=yd1]").val()) * 100;
    var zd2 = -40 + z0 + parseInt($(".form-task input[name=zd2]").val()) * 40;
    var xd2 = zd2 + x0 + parseInt($(".form-task input[name=xd2]").val()) * 100 + 40;
    var yd2 = -zd2 + y0 - parseInt($(".form-task input[name=yd2]").val()) * 100 - 40;

    var ze1 = z0 + parseInt($(".form-task input[name=ze1]").val()) * 40;
    var xe1 = ze1 + x0 + parseInt($(".form-task input[name=xe1]").val()) * 100;
    var ye1 = -ze1 + y0 - parseInt($(".form-task input[name=ye1]").val()) * 100;
    var ze2 = -40 + z0 + parseInt($(".form-task input[name=ze2]").val()) * 40;
    var xe2 = ze2 + x0 + parseInt($(".form-task input[name=xe2]").val()) * 100 + 40;
    var ye2 = -ze2 + y0 - parseInt($(".form-task input[name=ye2]").val()) * 100 - 40;

    var zf1 = z0 + parseInt($(".form-task input[name=zf1]").val()) * 40;
    var xf1 = zf1 + x0 + parseInt($(".form-task input[name=xf1]").val()) * 100;
    var yf1 = -zf1 + y0 - parseInt($(".form-task input[name=yf1]").val()) * 100;
    var zf2 = -40 + z0 + parseInt($(".form-task input[name=zf2]").val()) * 40;
    var xf2 = zf2 + x0 + parseInt($(".form-task input[name=xf2]").val()) * 100 + 40;
    var yf2 = -zf2 + y0 - parseInt($(".form-task input[name=yf2]").val()) * 100 - 40;






    var zL1 = z0 + parseInt($("input[name=zl1]").val()) * 40;
    var xL1 = zL1 + x0 + parseInt($("input[name=xl1]").val()) * 100;
    var yL1 = -zL1 + y0 - parseInt($("input[name=yl1]").val()) * 100;

    var zL2 = z0 + parseInt($("input[name=zl2]").val()) * 40;
    var xL2 = zL2 + x0 + parseInt($("input[name=xl2]").val()) * 100;
    var yL2 = -zL2 + y0 - parseInt($("input[name=yl2]").val()) * 100;

    var zL3 = z0 + parseInt($("input[name=zl3]").val()) * 40;
    var xL3 = zL3 + x0 + parseInt($("input[name=xl3]").val()) * 100;
    var yL3 = -zL3 + y0 - parseInt($("input[name=yl3]").val()) * 100;

    var zL4 = z0 + parseInt($("input[name=zl4]").val()) * 40;
    var xL4 = zL4 + x0 + parseInt($("input[name=xl4]").val()) * 100;
    var yL4 = -zL4 + y0 - parseInt($("input[name=yl4]").val()) * 100;

    var canvas = document.getElementById("canvas");
    if (canvas.getContext) {
        var ctx = canvas.getContext("2d");
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        ctx.fillStyle = "#00F";
        ctx.font = "12pt Arial";

        // Отрисовка направляющих
        ctx.beginPath();
        ctx.lineWidth = 1;
        ctx.beginPath();
        ctx.strokeStyle = "green";
        ctx.moveTo(x0, 0);
        ctx.lineTo(x0, 500);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "red";
        ctx.moveTo(0, y0);
        ctx.lineTo(500, y0);
        ctx.stroke();
        ctx.beginPath();
        ctx.strokeStyle = "blue";
        ctx.moveTo(x0 - 500, y0 + 500);
        ctx.lineTo(x0 + 500, y0 - 500);
        ctx.stroke();

        // Отрисовка куба
        ctx.beginPath();
        ctx.lineWidth = 3;
        ctx.strokeStyle = "black";
        ctx.moveTo(xa1, ya1);
        ctx.lineTo(xa2, ya2);
        ctx.moveTo(xb1, yb1);
        ctx.lineTo(xb2, yb2);
        ctx.moveTo(xc1, yc1);
        ctx.lineTo(xc2, yc2);
        ctx.moveTo(xd1, yd1);
        ctx.lineTo(xd2, yd2);
        ctx.moveTo(xe1, ye1);
        ctx.lineTo(xe2, ye2);
        ctx.moveTo(xf1, yf1);
        ctx.lineTo(xf2, yf2);

        ctx.moveTo(xa1, ya1);
        ctx.lineTo(xb1, yb1);
        ctx.moveTo(xb1, yb1);
        ctx.lineTo(xc1, yc1);
        ctx.moveTo(xc1, yc1);
        ctx.lineTo(xd1, yd1);
        ctx.moveTo(xd1, yd1);
        ctx.lineTo(xe1, ye1);
        ctx.moveTo(xe1, ye1);
        ctx.lineTo(xf1, yf1);
        ctx.moveTo(xf1, yf1);
        ctx.lineTo(xa1, ya1);

        ctx.moveTo(xa2, ya2);
        ctx.lineTo(xb2, yb2);
        ctx.moveTo(xb2, yb2);
        ctx.lineTo(xc2, yc2);
        ctx.moveTo(xc2, yc2);
        ctx.lineTo(xd2, yd2);
        ctx.moveTo(xd2, yd2);
        ctx.lineTo(xe2, ye2);
        ctx.moveTo(xe2, ye2);
        ctx.lineTo(xf2, yf2);
        ctx.moveTo(xf2, yf2);
        ctx.lineTo(xa2, ya2);

        ctx.stroke();
        ctx.fillText("A1", xa1 - 20, ya1 - 5);
        ctx.fillText("A2", xa2 - 20, ya2 - 5);
        ctx.fillText("B1", xb1 - 20, yb1 - 5);
        ctx.fillText("B2", xb2 - 20, yb2 - 5);
        ctx.fillText("C1", xc1 - 20, yc1 - 5);
        ctx.fillText("C2", xc2 - 20, yc2 - 5);
        ctx.fillText("D1", xd1 - 20, yd1 - 5);
        ctx.fillText("D2", xd2 - 20, yd2 - 5);
        ctx.fillText("E1", xe1 - 20, ye1 - 5);
        ctx.fillText("E2", xe2 - 20, ye2 - 5);
        ctx.fillText("F1", xf1 - 20, yf1 - 5);
        ctx.fillText("F2", xf2 - 20, yf2 - 5);



        // Отрисовка прямых
        ctx.beginPath();
        ctx.lineWidth = 5;
        ctx.fillStyle = "red";
        ctx.strokeStyle = "red";
        ctx.moveTo(xL1, yL1);
        ctx.lineTo(xL2, yL2);
        ctx.moveTo(xL3, yL3);
        ctx.lineTo(xL4, yL4);
        ctx.stroke();
        ctx.fillText("L1", xL1 + 5, yL1 + 20);
        ctx.fillText("L2", xL2 + 5, yL2 + 20);
        ctx.fillText("L3", xL3 + 5, yL3 + 20);
        ctx.fillText("L4", xL4 + 5, yL4 + 20);
    }
}
