<?php if ($_SESSION['user']['role'] == 1) : ?>
    <div class="row">
        <h2 class="display-3">Добавление нового задания</h2>
        <div class="col-12">
            <form id="FormTaskCreate" action="/?page=task-create" method="post" class="form-task">
                <div class="form-group">
                    <label for="TextareaFormTaskCreateText">Текст задания</label>
                    <textarea class="form-control" id="TextareaFormTaskCreateText" name="text" cols="30" rows="2" required></textarea>
                </div>
                <div class="form-group row">
                    <label for="InputFormTaskCreateAnswer" class="col-sm-2 col-form-label">Правильный ответ</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="answer" id="InputFormTaskCreateAnswer" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-12 col-md-6 d-flex flex-column">
                        <h5>Прямая 1</h5>
                        <div class="d-flex">
                            <p class="m-1 mr-3"><strong>L1</strong></p>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xl1" class="form-control" value="0" required>
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yl1" class="form-control" value="0" required>
                                <label for="">y</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zl1" class="form-control" value="0" required>
                                <label for="">z</label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <p class="m-1 mr-3"><strong>L2</strong></p>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xl2" class="form-control" value="0" required>
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yl2" class="form-control" value="0" required>
                                <label for="">y</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zl2" class="form-control" value="0" required>
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-6 d-flex flex-column">
                        <h5>Прямая 2</h5>
                        <div class="d-flex">
                            <p class="m-1 mr-3"><strong>L3</strong></p>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xl3" class="form-control" value="0" required>
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yl3" class="form-control" value="0" required>
                                <label for="">y</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zl3" class="form-control" value="0" required>
                                <label for="">z</label>
                            </div>
                        </div>
                        <div class="d-flex">
                            <p class="m-1 mr-3"><strong>L4</strong></p>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xl4" class="form-control" value="0" required>
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yl4" class="form-control" value="0" required>
                                <label for="">y</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zl4" class="form-control" value="0" required>
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xA<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xa1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ya1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="za1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>

                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xA<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xa2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ya2" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="za2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xB<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xb1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yb1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zb1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xB<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xb2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yb2" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zb2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xC<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xc1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yc1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zc1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xC<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xc2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yc2" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zc2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xD<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xd1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yd1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zd1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xD<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xd2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yd2" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zd2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xE<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xe1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ye1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ze1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xE<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xe2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ye2" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="ze2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xF<sub>1</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xf1" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yf1" value="0" required class="form-control">
                                <label for="">y</label>

                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zf1" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12 col-md-6">
                        <div class="col-md-2">
                            <h5>xF<sub>2</sub></h5>
                        </div>
                        <div class="col-md-10 d-flex">
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="xf2" value="0" required class="form-control">
                                <label for="">x</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="yf2" value="0" required class="form-control">
                                <label for="">y</label>
                            </div>
                            <div class="d-flex flex-column align-items-center">
                                <input type="number" name="zf2" value="0" required class="form-control">
                                <label for="">z</label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="d-flex justify-content-center align-items-center bg-light">
                    <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
                </div>
                <input type="hidden" name="user" value="<?= $_SESSION['user']['id'] ?>">
                <div class="d-flex justify-content-center align-items-center">
                    <button type="submit" name="form-task-create" class="btn btn-lg btn-primary mt-3 pr-3 pl-3">Создать задание</button>
                </div>

            </form>
        </div>
    </div>
<?php else : ?>
    <div class="row site-style-default pt-3 pb-3">
        <div class="col-md-4 d-flex justify-content-center align-items-center">
            <img src="assets/img/icon-forbidden.png" style="height: 250px;" alt="">
        </div>
        <div class="col-md-8 d-flex flex-column justify-content-center align-items-center">
            <h3 class="display-3 m-3 text-center">
                Доступ запрещен
            </h3>
            <p class="mt-1">
                Добавлять новые задания может только преподаватель
            </p>
        </div>
    </div>
    <div class="row">
        <div class="col-12 d-flex justify-content-center">
            <a href="/" class="btn btn-lg btn-primary m-3 p-3">На главную</a>
        </div>
    </div>
<?php endif ?>