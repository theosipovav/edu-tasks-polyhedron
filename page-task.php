<?php
global $db;
$get = $_GET;
$task = $db->getTask($get['id']);
$answer = $db->getAnswerByUserAndTask($_SESSION['user']['id'], $task->id);
?>
<div class="row">
    <div class="col-12">
        <h2>Задача №<?= $task->id ?></h2>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <p class="lead"><?= $task->text ?></p>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-12">
        <form class="container form-task">
            <div class="row">
                <div class="col-12 col-md-6 d-flex flex-column">
                    <h5>Прямая 1</h5>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L1</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl1" value="<?= $task->xl1 ?>" class="form-control">
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl1" value="<?= $task->yl1 ?>" class="form-control">
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl1" value="<?= $task->zl1 ?>" class="form-control">
                            <label for="">z</label>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L2</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl2" value="<?= $task->xl2 ?>" class="form-control">
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl2" value="<?= $task->yl2 ?>" class="form-control">
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl2" value="<?= $task->zl2 ?>" class="form-control">
                            <label for="">z</label>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6 d-flex flex-column">
                    <h5>Прямая 2</h5>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L3</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl3" value="<?= $task->xl3 ?>" class="form-control">
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl3" value="<?= $task->yl3 ?>" class="form-control">
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl3" value="<?= $task->zl3 ?>" class="form-control">
                            <label for="">z</label>
                        </div>
                    </div>
                    <div class="d-flex">
                        <p class="m-1 mr-3"><strong>L4</strong></p>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="xl4" value="<?= $task->xl4 ?>" class="form-control">
                            <label for="">x</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="yl4" value="<?= $task->yl4 ?>" class="form-control">
                            <label for="">y</label>
                        </div>
                        <div class="d-flex flex-column align-items-center">
                            <input type="number" name="zl4" value="<?= $task->zl4 ?>" class="form-control">
                            <label for="">z</label>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="col-12 col-md-7">
        <div class="d-flex flex-column">
            <canvas id="canvas" style="border: none;" width="500" height="500"></canvas>
            <div class="card border-primary mt-3 mb-3">
                <div class="card-header">Ответ</div>
                <div class="card-body text-primary">
                    <?php if ($answer == false) : ?>
                        <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerCreate" method="post" class="form-inline">
                            <div class="col-md-8 d-flex">
                                <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
                                <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult">
                                <input type="hidden" name="user" value="<?= $_SESSION['user']['id'] ?>">
                                <input type="hidden" name="task" value="<?= $task->id ?>">
                            </div>
                            <div class="col-md-4 d-flex flex-column">
                                <button type="submit" name="form-answer-create" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    <?php else : ?>
                        <form action="/?page=task&id=<?= $task->id ?>" id="FormAnswerUpdate" method="post" class="form-inline">
                            <div class="col-md-8 d-flex">
                                <label for="InputFormAnswerResult" class="mr-3" style="min-width: 100px;">Ваш ответ:</label>
                                <input class="form-control flex-grow-1" type="text" name="res" id="InputFormAnswerResult" value="<?= $answer->res ?>">
                                <input type="hidden" name="id" value="<?= $answer->id ?>">
                            </div>
                            <div class="col-md-4 d-flex flex-column">
                                <button type="submit" name="form-answer-update" class="btn btn-primary">Обновить</button>
                            </div>
                        </form>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12 col-md-5">
        <form class="container form-task">
            <input type="hidden" id="inputTask" value="<?= $task->id ?>">
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xA<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xa1" value="<?= $task->xa1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ya1" value="<?= $task->ya1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="za1" value="<?= $task->za1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xA<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xa2" value="<?= $task->xa2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ya2" value="<?= $task->ya2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="za2" value="<?= $task->za2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xB<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xb1" value="<?= $task->xb1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yb1" value="<?= $task->yb1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zb1" value="<?= $task->zb1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xB<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xb2" value="<?= $task->xb2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yb2" value="<?= $task->yb2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zb2" value="<?= $task->zb2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xC<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xc1" value="<?= $task->xc1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yc1" value="<?= $task->yc1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zc1" value="<?= $task->zc1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xC<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xc2" value="<?= $task->xc2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yc2" value="<?= $task->yc2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zc2" value="<?= $task->zc2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xD<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xd1" value="<?= $task->xd1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yd1" value="<?= $task->yd1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zd1" value="<?= $task->zd1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xD<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xd2" value="<?= $task->xd2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yd2" value="<?= $task->yd2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zd2" value="<?= $task->zd2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xE<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xe1" value="<?= $task->xe1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ye1" value="<?= $task->ye1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ze1" value="<?= $task->ze1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xE<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xe2" value="<?= $task->xe2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ye2" value="<?= $task->ye2 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="ze2" value="<?= $task->ze2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xF<sub>1</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xf1" value="<?= $task->xf1 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yf1" value="<?= $task->yf1 ?>" class="form-control">
                        <label for="">y</label>

                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zf1" value="<?= $task->zf1 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
            <div class="form-row">
                <div class="col-md-2">
                    <h5>xF<sub>2</sub></h5>
                </div>
                <div class="col-md-10 d-flex">
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="xf2" value="<?= $task->xf2 ?>" class="form-control">
                        <label for="">x</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="yf2" value="<?= $task->yf2 ?>" class="form-control">
                        <label for="">y</label>
                    </div>
                    <div class="d-flex flex-column align-items-center">
                        <input type="number" name="zf2" value="<?= $task->zf2 ?>" class="form-control">
                        <label for="">z</label>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>