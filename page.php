<?php
global $isLogged;
// Маршрутизация
$fileRoute = 'page-default.php';
if (isset($_GET["page"])) {
    $route = $_GET["page"];
    if ($isLogged) {
        $fileRoute = 'page-' . $route . '.php';
    } else {
        $fileRoute = 'page-forbidden.php';
    }
    if ($route == 'login') {
        $fileRoute = 'page-' . $route . '.php';
    }
    if ($route == 'reg') {
        $fileRoute = 'page-' . $route . '.php';
    }
    if ($route == 'logout') {
        $fileRoute = 'page-' . $route . '.php';
    }
}
if (!file_exists($fileRoute)) {
    $fileRoute = 'page-404.php';
}

?>
<div class="container mt-3">
    <?php include_once $fileRoute; ?>
</div>