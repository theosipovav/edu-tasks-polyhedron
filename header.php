<?php
global $messageSuccess;
global $messageWarning;
global $messageError;


?>
<nav class="navbar navbar-expand-lg navbar-light bg-white site-style-default">
    <a class="navbar-brand" href=".">
        <img src="assets/img/logo.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
        Многогранники <sub>задания</sub>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="/">Главная <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?page=tasks">Задания</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?page=task-create">Добавить задание</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/?page=answers">Результат</a>
            </li>
        </ul>
        <div class="my-2 my-lg-0 d-flex justify-content-center align-items-center">
            <?php if ($isLogged) : ?>
                <button type="submit" name="form-logout" form="FormPost" class="btn btn-link text-secondary" title="Выход">
                    <i class="fas fa-sign-out-alt"></i>
                </button>
                <a href="/?page=profile" class="btn btn-link text-secondary" title="Профиль">
                    <i class="fas fa-user"></i>
                </a>
            <?php else : ?>
                <button class="btn btn-link text-secondary" data-toggle="modal" data-target="#ModalLogIn" title="Вход">
                    <i class="fas fa-sign-in-alt"></i>
                </button>
                <button class="btn btn-link text-secondary" data-toggle="modal" data-target="#ModalRegistration" title="Регистрация">
                    <i class="fas fa-user-plus"></i>
                </button>
            <?php endif ?>
        </div>
    </div>
</nav>
<?php
include_once 'content-login.php';
include_once 'content-registration.php';
?>

<div class="container">
    <?php if ($messageSuccess != "") : ?>
        <div class="card text-white bg-success mt-3 mb-3">
            <div class="card-header">Успешно!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageSuccess ?></p>
            </div>
        </div>
    <?php endif ?>
    <?php if ($messageError != "") : ?>
        <div class="card text-white bg-danger mt-3 mb-3">
            <div class="card-header">Ошибка!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageError ?></p>
            </div>
        </div>
    <?php endif ?>
    <?php if ($messageWarning != "") : ?>
        <div class="card text-white bg-warning mt-3 mb-3">
            <div class="card-header">Внимание!</div>
            <div class="card-body">
                <p class="card-text"><?= $messageWarning ?></p>
            </div>
        </div>
    <?php endif ?>
</div>


<form id="FormPost" action="/" method="post"></form>