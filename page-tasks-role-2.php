<?php
global $db;


$group = $db->getGroupById($_SESSION['user']['group']);
$tasks = getTaskForGroup($group->id);
?>
<div class="row">
    <div class="col-12">
        <h2>Задания учебной группы <span><?= $group->name ?></span></h2>
    </div>
</div>
<div class="row">
    <?php foreach ($tasks as $key => $task) : ?>
        <div class="p-1 col-12 col-sm-6 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Задание №<?= $task->id ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted">Учебная группа: <?= $db->getGroupById($db->getUserById($task->author)->group)->name ?></h6>
                    <div class="d-flex justify-content-end text-secondary">
                        <?= $task->created_dt ?>
                    </div>
                    <hr>
                    <div class="d-flex flex-grow-1 justify-content-center">
                        <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-primary">Перейти</a>
                        <?php if ($_SESSION['user']['role'] == 1) : ?>
                            <a href="/?page=task-remove&id=<?= $task->id ?>" class="btn btn-danger ml-1">Удалить</a>
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>