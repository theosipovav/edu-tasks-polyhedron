<?php

global $db;

$groups = $db->getGroups();
$roles = $db->getRoles();


$groupCurrent = $db->getGroupById($_SESSION['user']['group']);
$roleCurrent = $db->getRole($_SESSION['user']['role']);


?>
<div class="card mb-3 site-style-default">
    <div class="row no-gutters">
        <div class="col-md-4">
            <?php if ($_SESSION['user']['role'] == 1) : ?>
                <img src="assets/img/icon-teacher.png" class="card-img-top profile-avatar m-1" alt="">
            <?php else : ?>
                <img src="assets/img/icon-student.png" class="card-img-top profile-avatar m-1" alt="">
            <?php endif ?>
        </div>
        <div class="col-md-8">
            <div class="card-body">
                <h5 class="card-title"><?= $_SESSION['user']['name'] ?></h5>

                <table class="table table-sm table-light">
                    <tbody>
                        <tr>
                            <td>ID</td>
                            <td><?= $_SESSION['user']['id'] ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Учебная группа:</th>
                            <td><?= $groupCurrent->name ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Электронная почты:</th>
                            <td><?= $_SESSION['user']['email'] ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Роль:</th>
                            <td><?= $roleCurrent->name ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Доступно заданий</th>
                            <td><?= count(getTaskForGroup($_SESSION['user']['group'])) ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Решено заданий</th>
                            <td><?= count($db->getAnswersByUser($_SESSION['user']['id'])) ?></td>
                        </tr>
                        <tr>
                            <th scope="row">Вами опубликовано задач</th>
                            <td><?= count($db->getTasksByAuthors([$_SESSION['user']['id']])) ?></td>
                        </tr>
                    </tbody>
                </table>
                <hr>
                <p class="card-text">
                    <span><small class="text-muted">Дата регистрации: </small></span>
                    <span><small class="text-muted"><?= $_SESSION['user']['created_dt'] ?></small></span>
                </p>
                <div class="d-flex flex-column align-items-center">
                    <button class="btn btn-outline-primary" style="width: 200px;" data-toggle="modal" data-target="#ModalEditProfile">
                        <i class="fas fa-user-edit"></i>
                        <span>Редактировать</span>
                    </button>
                    <button type="submit" form="FormPost" name="form-logout" class="btn btn-secondary mt-1" style="width: 200px;" title="Выход">
                        <i class="fas fa-sign-out-alt"></i>
                        <span>Выход</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="ModalEditProfile" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="ModalEditProfileLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalEditProfileLabel">Редактирование профиля</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="FormProfileUpdate" action="/?page=profile" method="POST">
                    <div class="form-group">
                        <label for="InputEditProfileEmail">Адрес электронной почты</label>
                        <input type="email" name="email" class="form-control" id="InputEditProfileEmail" placeholder="name@mail.ru" value="<?= $_SESSION['user']['email'] ?>" required>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="InputEditProfilePassword">Пароль</label>
                            <input type="password" name="password" class="form-control" id="InputEditProfilePassword" value="<?= $_SESSION['user']['password'] ?>" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="InputEditProfilePasswordRepeat">Повторите пароль</label>
                            <input type="password" class="form-control" id="InputEditProfilePasswordRepeat" value="<?= $_SESSION['user']['password'] ?>" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="InputEditProfileName">Полное имя</label>
                        <input type="text" class="form-control" id="InputEditProfileName" name="name" placeholder="Иванов Иван Иванович" value="<?= $_SESSION['user']['name'] ?>" required>
                    </div>
                    <div class="form-row">
                        <div class="col-12 col-md-6">
                            <label for="SelectEditProfileGroup">Ваша учебная группа</label>
                            <select id="SelectEditProfileGroup" class="custom-select" name="group" required>
                                <?php foreach ($groups as $key => $group) : ?>
                                    <?php if ($group->id == $groupCurrent->id) : ?>
                                        <option selected value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php else : ?>
                                        <option value="<?= $group->id ?>"><?= $group->name ?></option>
                                    <?php endif ?>
                                <?php endforeach ?>
                            </select>
                        </div>
                        <div class="col-12 col-md-6">
                            <label>Ваша роль</label>
                            <?php foreach ($roles as $key => $role) : ?>
                                <div class="form-check ml-3">
                                    <?php if ($role->id == $roleCurrent->id) : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>" checked>
                                        <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php else : ?>
                                        <input class="form-check-input" type="radio" name="role" id="RadioEditProfileRole<?= $role->id ?>" value="<?= $role->id ?>">
                                        <label class="form-check-label" for="RadioEditProfileRole<?= $role->id ?>">
                                            <?= $role->name ?>
                                        </label>
                                    <?php endif ?>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <input type="hidden" name="id" value="<?= $_SESSION['user']['id'] ?>">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Отмена</button>
                <button type="submit" name="form-profile-update" form="FormProfileUpdate" class="btn btn-primary">Далее</button>
            </div>
        </div>
    </div>
</div>