<?php
global $db;

$tasks = $db->getTasks();

?>

<div class="row">
    <?php foreach ($tasks as $key => $task) : ?>
        <div class="p-1 col-12 col-sm-6 col-lg-4">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Задание №<?= $task->id ?></h5>
                    <h6 class="card-subtitle mb-2 text-muted">Учебная группа: <?= $db->getGroupById($db->getUserById($task->author)->group)->name ?></h6>
                    <div class="d-flex justify-content-end text-secondary">
                        <?= $task->created_dt ?>
                    </div>
                    <hr>
                    <div class="d-flex flex-grow-1 justify-content-center">
                        <a href="/?page=task&id=<?= $task->id ?>" class="btn btn-primary">Перейти</a>
                        <?php if ($_SESSION['user']['role'] == 1) : ?>
                            <form id="FormTaskRemove<?= $task->id ?>" action="/?page=tasks" method="post">
                                <input type="hidden" name="id" value="<?= $task->id ?>">
                            </form>
                            <button type="submit" form="FormTaskRemove<?= $task->id ?>" name="form-task-remove" class="btn btn-danger btn-confirm ml-1">Удалить</button>
                        <?php endif ?>
                    </div>

                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>