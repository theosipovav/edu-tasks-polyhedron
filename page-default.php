<?php
global $db;
?>

<div class="row">
    <div class="col-12 text-center">
        <h1 class="display-1">Добро пожаловать!</h1>
        <hr>
    </div>
</div>
<?php if ($isLogged) : ?>
    <div class="row">

        <div class="col-12">
            <div class="card site-style-default">
                <div class="card-body">
                    <h5 class="card-title">Статистика</h5>
                    <div class="d-flex flex-column">
                        <?php if ($_SESSION['user']['role'] == 1) : ?>
                            <p class="card-text">Вами опубликовано <?= count($db->getTasksByAuthors([$_SESSION['user']['id']])) ?> задач</p>
                        <?php else : ?>
                            <p class="card-text">Вам доступно <?= count(getTaskForGroup($_SESSION['user']['group'])) ?> задач</p>
                            <p class="card-text">Вы решили <?= count($db->getAnswersByUser($_SESSION['user']['id'])) ?> задач</p>
                        <?php endif ?>
                    </div>

                </div>
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card site-style-default">
                <div class="card-body">
                    <h5 class="card-title">Пример решения задачи</h5>
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner carousel-examples">
                            <div class="carousel-item active">
                                <img src="assets/img/example-1.jpg" class="carousel-examples--img d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/example-2.jpg" class="carousel-examples--img d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/example-3.jpg" class="carousel-examples--img d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/example-4.jpg" class="carousel-examples--img d-block w-100" alt="...">
                            </div>
                            <div class="carousel-item">
                                <img src="assets/img/example-5.jpg" class="carousel-examples--img d-block w-100" alt="...">
                            </div>
                        </div>
                        <a class="carousel-control-prev text-dark" style="font-size: 3rem;" href="#carouselExampleControls" role="button" data-slide="prev">
                            <i class="fas fa-chevron-left"></i>
                        </a>
                        <a class="carousel-control-next text-dark" style="font-size: 3rem;" href="#carouselExampleControls" role="button" data-slide="next">
                            <i class="fas fa-chevron-right"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php else : ?>
    <div class="row">
        <div class="col-12">
            <div class="jumbotron site-style-default">
                <p class="lead">Уважаемый пользователь, для неавторизованных пользователь функционал сайта недоступен</p>
                <hr class="my-4">
                <p>Пожалуйста, <button class="btn btn-link" data-toggle="modal" data-target="#ModalLogIn" style="display: contents;">войдите</button> на сайт под своими учетными данными или пройдите <button class="btn btn-link" data-toggle="modal" data-target="#ModalRegistration" style="display: contents;">регистрацию</button> на сайте</p>
            </div>
        </div>
    </div>
<?php endif ?>